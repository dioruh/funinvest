import * as styledComponents from "styled-components";

const {
	default: styled,
	css,
	keyframes,
	ThemeProvider
} = styledComponents as styledComponents.ThemedStyledComponentsModule<
	IThemeInterface
	>;

export interface IThemeInterface {
	primary: string;
	secondary: string;
	darkPurple: string;
	lightPurple: string;
	white: string;
	error: string;
}

export const theme = {
	primary: "rgb(75, 21, 144)",
	secondary: "rgb(183, 5, 245)",
	darkPurple: "rgb(25, 0, 52)",
	lightPurple: "rgb(199, 153, 255)",
	white: "rgb(255, 255, 255)",
	red: "rgb(217, 33, 33)",
};

export default styled;
export { css, keyframes, ThemeProvider };
