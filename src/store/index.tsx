import {createStore} from 'redux';

export interface Brand {
	name: string;
	percentage: number;
	value: number;
	description: string;
	source: string;
}

export interface state {
	brands: Brand[];
	investments: Brand[];
}

interface BrandsAction {
	item?: any;
	index?: number;
	type: string;
	brand: Brand;
	name: string;
}

const INITIAL_STATE: state = {
	brands: [
		{
			name: "Google",
			percentage: 0.015,
			value: 12.5,
			description: "Google LLC is an American multinational technology company that specializes in Internet-related services and products, which include online advertising technologies, search engine, cloud computing, software, and hardware.",
			source: "https://upload.wikimedia.org/wikipedia/commons/5/53/Google_%22G%22_Logo.svg"
		},
		{
			name: "Yandex",
			percentage: 0.33,
			value: 871.4,
			description: "Yandex N.V. is a Russian multinational corporation specializing in Internet-related products and services, including transportation, search and information services, eCommerce, navigation, mobile applications, and online advertising.",
			source: "https://upload.wikimedia.org/wikipedia/commons/8/80/Yandex_Browser_logo.svg"
		},
		{
			name: "Huawei",
			percentage: 0.23,
			value: 158.1,
			description: "Huawei Technologies Co., Ltd. is a Chinese multinational technology company. It provides telecommunications equipment and sells consumer electronics, smartphones and is headquartered in Shenzhen, Guangdong.",
			source: "https://upload.wikimedia.org/wikipedia/en/0/04/Huawei_Standard_logo.svg"
		}
	],
	investments: []
};

function brands(state = INITIAL_STATE, action: BrandsAction) {
	switch (action.type) {
		case 'ADD_INVESTMENT':
			return {...state, investments: [...state.investments, { ...action.item }]};
		case 'REMOVE_INVESTMENT':
			state.investments.splice(Number(action.index), 1);
			return {...state};
		case 'GET_BRAND':
			return state.brands.find(brand => brand.name === action.name);
		default:
			return INITIAL_STATE;
	}
}

// @ts-ignore
const store = createStore(brands);

export default store;
