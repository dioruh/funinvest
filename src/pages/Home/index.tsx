import React from "react";
import Header from "../../components/Header";
import MainContent from "../../components/MainContent";
import Footer from "../../components/Footer";
import Savings from "../../assets/savings.svg";
import styled from "../../theme";
import SideContent from "../../components/SideContent";

const Container = styled.div`
	padding: 2em;
	display: flex;
	align-items: center;
	justify-content: center;
`;

const Wrapper = styled.div`
	display: flex;
`;

export default function Home() {
	return (
		<Wrapper>
			<MainContent>
				<Header/>
				<Container>
					<img alt="Main" src={Savings} style={{ width: "80%" }}/>
				</Container>
				<Footer/>
			</MainContent>
			<SideContent/>
		</Wrapper>
	)
}
