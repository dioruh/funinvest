import React from "react";
import styled, {theme} from "../../theme";

const Container: React.FC = ({children}) => {
	return <div>{children}</div>
};

const Text = styled.span`
	color: ${theme.white};
	position: relative;
	padding: 2em;
	font-size: 11px;
`;

export default function Footer() {
	return (
		<Container>
			<Text>© 2020 John Doe.  All rights reserved.</Text>
		</Container>
	)
}
