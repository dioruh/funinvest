import React from "react";
import styled, {theme} from "../../theme";
import Card from "../Card";
import {useSelector} from "react-redux";
import {state} from "../../store";
import NewInvestmentCard from "../Card/NewInvestment";

const Container = styled.div`
	background: ${theme.primary};
	width: 35%;
	padding: 2em;
`;

const NewInvestmentsTitle = styled.h1`
	color: ${theme.white};
`;

const NewInvestmentsContainer = styled.div`
	height: 50%;
	display: flex; 
  flex-wrap: nowrap;
  overflow-x: auto;
	&::-webkit-scrollbar-track { background-color: ${theme.primary}; };
	&::-webkit-scrollbar { width: 0.5rem; background-color: #F5F5F5; };
	&::-webkit-scrollbar-thumb { border-radius: 0.625rem; background-color: ${theme.lightPurple}; }
`;

const ProvenInvestmentsTitle = styled.h1`
	color: ${theme.white};
`;
const Empty = styled.span`
	color: ${theme.white};
	opacity: 0.5;
`;
const ProvenInvestmentsContainer = styled.div`
	margin-top: 1em;
	height: 30%;
	display: flex;
	align-content: center;
	justify-content: center;
`;

export default function SideContent() {
	const {brands, investments} = useSelector((state: state) => state);

	const listInvestments = () => {
		if (!investments.length) return <Empty>No investments found.</Empty>;
		return investments?.map((investment, index) => <NewInvestmentCard
			key={`${index}-${investment.name}`}
			index={index}
			{...investment}
		/>)
	};

	return (
		<Container>
			<NewInvestmentsTitle>New Investments</NewInvestmentsTitle>
			<NewInvestmentsContainer>
				{listInvestments()}
			</NewInvestmentsContainer>
			<ProvenInvestmentsTitle>Proven Investments</ProvenInvestmentsTitle>
			<ProvenInvestmentsContainer>
				{
					brands?.map((brand) => <Card key={brand.name} source={brand.source} name={brand.name} {...brand}/>)
				}
			</ProvenInvestmentsContainer>
		</Container>
	)
}
