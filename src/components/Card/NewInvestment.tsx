import React from "react";
import styled, {theme} from "../../theme";
import Button from "../Button";
import {useDispatch} from "react-redux";

interface Props {
	index: number,
	source?: string,
	name?: string,
	description?: string,
	value?: number,
	percentage?: number
}

const Wrapper = styled.div`
	background: ${theme.darkPurple};
	border-radius: 1.25rem;
	height: 20rem;
	flex: 0 0 auto;
	width: 80%;
	padding: 0.75rem;
	margin-bottom: 0.375rem;
	margin-right: 0.75rem;	
	flex-wrap: wrap;
`;
const Container = styled.div`
	background: ${theme.primary};
	margin: 0.75rem;
	padding: 0.75rem;
	border-radius: 1.25rem;
	display: flex;
`;
const LogoContainer = styled.div``;
const DetailsContainer = styled.div`
	width: 60%;
	max-width: 60%;
	padding-left: 0.5rem;
	display: flex;
	flex-wrap: wrap;
`;
const Footer = styled.div`
	margin: 0.75rem;
	padding: 0.75rem; 
	display: flex;
	justify-content: space-around;
`;
const Title = styled.span`
	color: ${theme.white};
	font-size: 1.5rem;
	font-weight: bolder;
`;
const Description = styled.span`
	color: ${theme.white};
	font-size: 0.875rem;
	opacity: 0.5;
`;
const Text = styled.span`
	color: ${theme.white};
	font-size: 1rem;
	width: 150%;
	margin-top: 1.5em;
`;

const NewInvestmentCard: React.FC<Props> = (props) => {
	const dispatch = useDispatch();

	function removeInvestment(index: number) {
		dispatch({ type: "REMOVE_INVESTMENT", index })
	}

	return (
		<Wrapper>
			<Container>
				<LogoContainer>
					<img
						src={props.source}
						alt={props.name}
						width={100}
						height={100}
						style={{ borderRadius: 50 }}
					/>
				</LogoContainer>
				<DetailsContainer>
					<Title>{props.name}</Title>
					<Description>
						{props.description}
					</Description>
				</DetailsContainer>
			</Container>
			<Footer>
				<Text>Value: ${props.value}</Text>
				<Text>Profit chance: {props.percentage ? props.percentage * 100 : 0 }%</Text>
				<Button label="Remove" color={theme.red} size="15px" onClick={() => removeInvestment(props.index)}/>
			</Footer>
		</Wrapper>
	)
};

export default NewInvestmentCard;
