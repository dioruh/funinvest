import React from "react";
import styled, {theme} from "../../theme";
import Button from "../Button";
import {useDispatch} from "react-redux";

interface Props {
	source: string,
	name: string,
	description: string
}

const Wrapper = styled.div`
	background: ${theme.darkPurple};
	border-radius: 1.25rem;
	height: 10.625rem;
	width: 7.5rem;
	padding: 0.75rem;
	text-align: center;
	margin-right: 12px;
`;

const Text = styled.span`
	color: ${theme.white};
	font-size: 1.25rem;
	font-weight: bolder;
`;

const ImageContainer = styled.div`
	padding: 0.75rem;
`;

const Card: React.FC<Props> = (props) => {
	const dispatch = useDispatch();

	function addInvestment(item: React.PropsWithChildren<Props>) {
		dispatch({ type: "ADD_INVESTMENT", item })
	}

	return (
		<Wrapper>
			<ImageContainer>
				<img src={props.source} alt={props.name} style={{ width: 50 }}/>
			</ImageContainer>
			<Text>{props.name}</Text>
			<Button onClick={() => addInvestment(props)}/>
		</Wrapper>
	)
};

export default Card;
