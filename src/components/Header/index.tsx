import React from "react";
import styled, {theme} from "../../theme";

const Container = styled.div`
	padding: 2em;
`;

const Title = styled.h1`
	color: ${theme.white};
	font-size: 6em;
	margin: 0;
`;

const SubTitle = styled.h3`
	color: ${theme.white};
	font-size: 1.5em;
	margin: 0;
`;

export default function Header() {
	return (
		<Container>
			<Title>FunInvest</Title>
			<SubTitle>Investing is fun.</SubTitle>
		</Container>
	)
}
