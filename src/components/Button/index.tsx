import React from "react";
import styled, {theme} from "../../theme";

interface Props {
	onClick: Function;
	color?: string;
	label?: string;
	size?: string;
}

const Container = styled.button`
	background: ${props => props.color || theme.secondary};
	border: none;
	border-radius: 1.25rem;
	width: 100%;
	padding: 1em;
	margin-top: 1.5em;
	cursor: pointer;
`;

const Text = styled.span`
	color: ${theme.white};
	font-size: 0.9375rem;
	font-weight: bold;
`;

const Button: React.FC<Props> = (props) => {
	return (
		<Container color={props.color} onClick={() => props.onClick()}>
			<Text>{props.label ? props.label : "Invest"}</Text>
		</Container>
	)
}

export default Button;
