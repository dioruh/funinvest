import styled, {theme} from "../../theme";

export const Container = styled.div`
	width: 65%;
	height: 100vh;
	background-color: ${theme.darkPurple};
`;
