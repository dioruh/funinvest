import React, {FunctionComponent} from "react";
import {Container} from "./styles";

const MainContent: FunctionComponent = ({ children }) => <Container>{children}</Container>;
export default MainContent;
